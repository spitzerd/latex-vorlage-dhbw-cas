function printChapters()
    for i=1,100,1 do
        local file = io.open("text/chapter"..i..".tex")
        if file ~= nil then 
            -- Datei existiert. Ausgabe der Datei chapterXX.tex und Seitenumbruch
            tex.print("\\input{text/chapter"..i..".tex}")
            tex.print("\\newpage")
            io.close(file)
        end
    end
end